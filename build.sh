#!/bin/bash

REPO_ROOT_PATH="$( realpath "$( dirname "$0" )" )"
cd "$REPO_ROOT_PATH"

# Clean build directory
[ -a ./build ] && rm -rf ./build
mkdir ./build/

pushd ./build/
ln -s "$REPO_ROOT_PATH"/src/pcistyle.{dtx,ins} \
      "$REPO_ROOT_PATH"/src/example_article.tex \
      "$REPO_ROOT_PATH"/src/example_article_ukr.tex \
      ./

# Prepare sample bibliography.
# Replace all em- and en-dashes with actual Unicode characters.
# sed 's/\([^-]\)--\([^-]\)/\1–\2/g;s/\([^-]\)---\([^-]\)/\1—\2/g'
for N in $({ seq -f '%02g' 37 ; echo 39 ; seq -f '%02g' 41 78 ; }) ;
do
    kpsewhich --path='$TEXMF' doc/bibtex/dstu/examples/utf8/bib/ex"$N".bib ; done \
        | xargs cat > example_article.bib

xelatex pcistyle.ins
xelatex pcistyle.dtx && xelatex pcistyle.dtx || exit "$?"


# Prepare English variant of example.
xelatex example_article.tex || exit "$?"

bibtexu -B -l ukr -o ukr example_article.aux
STATUS="$?"

[[ "$STATUS" -lt 128 ]] && xelatex example_article.tex && xelatex example_article.tex || exit "$?"

# Prepare Ukrainian variant of example.
xelatex example_article_ukr.tex || exit "$?"

bibtexu -B -l ukr -o ukr example_article_ukr.aux
STATUS="$?"

[[ "$STATUS" -lt 128 ]] && xelatex example_article_ukr.tex && xelatex example_article_ukr.tex || exit "$?"

mkdir -p tex/latex/pcistyle/ doc/tex/latex/pcistyle/
ln pcistyle.cls tex/latex/pcistyle/
ln *.pdf ../src/example_article*.tex doc/tex/latex/pcistyle/

# Bundle build results.
rm ../pcistyle.zip 2>/dev/null
zip --compression-method store --no-dir-entries --no-wild -X \
    ../pcistyle.zip \
    -- \
    tex/latex/pcistyle/* \
    doc/tex/latex/pcistyle/*
